<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Awesome Default Theme
 * @since Awesome Default Theme 3.0
 */
global $theme_options;

?>

</div><!-- #main .site-main -->

<footer class="site-footer" role="contentinfo">
	<div class="footer-content">
		<div class="container">
			<div id="footer-sidebar" class="row">
				<?php dynamic_sidebar( 'sidebar-footer' ); ?>
			</div>
		</div>
	</div> 

	<!-- .footer-info -->

	<div class="footer-info">

		<div class="container">

			<div class="row">
                <div class="col-md-12 footer-menu">
                   
                   <div class="col-xs-6 col-md-3 col-lg-3">                      
                       <div class="col-md-12">
                           <h6>Menu</h6>
                           <?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class'=>'pull-left', 'container' => 'ul','fallback_cb' => '', ) ); ?>
                       </div>
                   </div>
                   <div class="col-xs-6 col-md-3 col-lg-3">                      
                       <div class="col-md-12">
                           <h6>Menu</h6>
                           <?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class'=>'pull-left', 'container' => 'ul','fallback_cb' => '', ) ); ?>
                       </div>
                   </div>
                   <div class="col-xs-6 col-md-3 col-lg-3">                      
                       <div class="col-md-12">
                           <h6>Menu</h6>
                           <?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class'=>'pull-left', 'container' => 'ul','fallback_cb' => '', ) ); ?>
                       </div>
                   </div>                   
                   <div class="col-xs-6 col-md-3 col-lg-3">                      
                      <div class="col-md-12 slogan-company">
                       	<img src="<?php echo $theme_options['site-logo']['url']; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> logo">
                        <h6><?php echo $theme_options['site-slogan']; ?></h6>
                       </div>
                   </div>
                   
                   <div class="col-xs-12 col-md-12 col-lg-12">
                       <div class="site-social col-md-12">
                            <hr>
                            <?php if($theme_options['social-facebook']){ ?>			    
                            <a class ="social-icon" href="<?php echo $theme_options['social-facebook']; ?>" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
                            <?php } ?>
                            <?php if($theme_options['social-twitter']){ ?>	
                            <a class ="social-icon"href="<?php echo $theme_options['social-twitter']; ?>" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
                            <?php } ?>
                            <?php if($theme_options['social-google-plus']){ ?>
                            <a class ="social-icon"href="<?php echo $theme_options['social-google-plus']; ?>" target="_blank"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a>
                            <?php } ?>
                            <?php if($theme_options['social-skype']){ ?>
                            <a class ="social-icon"href="<?php echo $theme_options['social-skype']; ?>" target="_blank"><i class="fa fa-skype fa-2x" aria-hidden="true"></i></a>
                            <?php } ?>
                            <?php if($theme_options['social-linkedin']){ ?>
                            <a class ="social-icon"href="<?php echo $theme_options['social-linkedin']; ?>" target="_blank"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a>
                            <?php } ?>
                            <?php if($theme_options['social-youtube']){ ?>
                            <a class ="social-icon"href="<?php echo $theme_options['social-youtube']; ?>" target="_blank"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i></a>
                            <?php } ?>
					    </div>
                   </div>
                    
                </div>
                <div class="col-md-12">
				    <!--/ Start Copyright Info /-->
                    <div class="col-md-6">
                        <div class="site-copyright">
                            <?php do_action( 'awesome_copyright' ); ?>
                        </div>
                    </div>
                    <!--/ End Copyright Info /-->

                    <div class="col-md-6 ">
                        <div class="site-info">						
                            <p><span>Website by</span> <a href="http://dsa-global.com/" target="_blank">DSA Global</a></p>
                        </div>

                    </div><!-- .site-info -->    
				</div>
				

			</div> 

		</div><!-- .container -->            

	</div><!-- END .footer-info -->



</footer><!-- #colophon .site-footer -->
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>

</body>
</html>