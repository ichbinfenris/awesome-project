<?php
function awesome_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri() ?>/lib/awesome-login/logo-login.png);
            padding-bottom: 0px;
            margin-bottom:0;
			background-size: auto auto;
			height: 80px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'awesome_login_logo' );

function awesome_login_logo_url() {
    return 'http://www.dosa.io/';
}
add_filter( 'login_headerurl', 'awesome_login_logo_url' );

function awesome_login_logo_url_title() {
    return 'DSA Global';
}
add_filter( 'login_headertitle', 'awesome_login_logo_url_title' );

function awesome_login_stylesheet() { ?>
    <link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo get_template_directory_uri() . '/lib/awesome-login/style-login.css'; ?>" type="text/css" media="all" />
    <?php }
add_action( 'login_enqueue_scripts', 'awesome_login_stylesheet' );