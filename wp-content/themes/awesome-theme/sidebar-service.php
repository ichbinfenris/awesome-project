<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Awesome Default Theme
 * @since Awesome Default Theme 1.0
 */
?>
<div id="sidebar" class="widget-area" role="complementary">
	<?php do_action( 'before_sidebar' ); ?>
	<?php dynamic_sidebar( 'sidebar-service' ); ?>
</div>
<!-- #secondary .widget-area --> 
